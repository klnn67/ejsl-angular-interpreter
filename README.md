[![pipeline status](https://git.thm.de/klnn67/ejsl-angular-interpreter/badges/master/pipeline.svg)](https://git.thm.de/klnn67/ejsl-angular-interpreter/commits/master)

# eJSL Angular Interpreter

:fire: https://ejsl-angular-interpreter.firebaseapp.com/ :fire:

## Credits

eJSL und jooMDD sind Projekte des Instituts für Informationswissenschaften (II):

https://www.mni.thm.de/forschung/institute-a-gruppen/ii/ii-projekte/5166-joomdd

https://github.com/thm-mni-ii/JooMDD

<img src="https://www.mni.thm.de/images/MNI_content/Forschung/Logos_Institute/ii-01.png" height=70 >
<img src="https://github.com/icampus/JooMDD/raw/master/img/Logo_b.png" height=70 >
<img src="https://github.com/thm-mni-ii/JooMDD/raw/master/img/eJSL_Logo_trans.png" height=70 >

## Screenshots

![](/uploads/a7dd7a6fdea0edfbde9607015c802d67/Screenshot_2018-08-28_13.07.47.png)
![](/uploads/73f9bac80793b71675b031978cbb21ad/Screenshot_2018-08-28_13.04.26.png)
![](/uploads/94c6cb84fdd7cf709a21f53b28e6c8a3/Screenshot_2018-08-28_13.05.08.png)