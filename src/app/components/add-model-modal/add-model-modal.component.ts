import { Component, OnInit } from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {ModelSelectComponent} from '../../sites/model-select/model-select.component';
import {FirebaseService} from '../../services/firebase.service';
import {EJSLModel} from '../../../ejsl-ast/EJSLModel';

@Component({
  selector: 'app-add-model-modal',
  templateUrl: './add-model-modal.component.html',
  styleUrls: ['./add-model-modal.component.css']
})
export class AddModelModalComponent implements OnInit {

  xmlString: string;

  constructor(public dialogRef: MatDialogRef<ModelSelectComponent>,
              public firebase: FirebaseService) {
    this.xmlString = '';
  }

  ngOnInit() {
  }

  onCancelClick(): void {
    this.dialogRef.close();
  }

  async onSaveClick(): Promise<void> {
    try {
      const xmi = new EJSLModel(await this.firebase.parseStringAsnyc(this.xmlString), '');
      this.firebase.saveModel(this.xmlString, xmi.name);
      this.dialogRef.close();
    } catch (e) {
      alert(e);
    }
  }
}
