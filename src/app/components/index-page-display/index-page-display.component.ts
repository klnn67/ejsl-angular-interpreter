import {AfterViewInit, ChangeDetectorRef, Component, ElementRef, Input, OnChanges} from '@angular/core';
import {MatDialog, MatTableDataSource, Sort} from '@angular/material';
import {IEntry} from '../../types/entry';
import {Subscription} from 'rxjs';
import {isNullOrUndefined} from 'util';
import {ActivatedRoute, Router} from '@angular/router';
import {FirebaseService} from '../../services/firebase.service';
import {EJSLAttribute} from '../../../ejsl-ast/EJSLAttribute';
import {EJSLIndexPage} from '../../../ejsl-ast/page/EJSLIndexPage';
import {EJSLExtensionType} from '../../../ejsl-ast/types/EJSLExtensionType';
import {EJSLLink} from '../../../ejsl-ast/link/EJSLLink';
import {EJSLSection} from '../../../ejsl-ast/EJSLSection';
import {EJSLPageType} from '../../../ejsl-ast/types/EJSLPageType';
import {Search} from '../../helper/Search';
import {CssService} from '../../services/css.service';
import {EJSLPage} from '../../../ejsl-ast/page/EJSLPage';
import {EJSLModule} from '../../../ejsl-ast/extension/EJSLModule';

@Component({
  selector: 'app-index-page-display',
  templateUrl: './index-page-display.component.html',
  styleUrls: ['./index-page-display.component.css'],
})
export class IndexPageDisplayComponent implements OnChanges, AfterViewInit {
  EJSLExtensionType = EJSLExtensionType;

  @Input() page: EJSLIndexPage;
  @Input() section: EJSLSection | null; // is set if component
  @Input() module: EJSLModule | null; // is set if module
  isNullOrUndefined = isNullOrUndefined;
  selectCollection: string[];
  search: Search;
  private _entitySubscription: Subscription = new Subscription();
  private _data: IEntry[];
  private _sortedData: IEntry[];
  private _dataSource: MatTableDataSource<IEntry>;
  private _loading: boolean;

  constructor(private activatedRoute: ActivatedRoute,
              public dialog: MatDialog,
              private changeDetectorRefs: ChangeDetectorRef,
              private firebaseService: FirebaseService,
              private router: Router,
              private css: CssService,
              private elem: ElementRef) {
    this.selectCollection = [];
    this.search = new Search();
    this._data = [];
    this._sortedData = [];
    this._dataSource = new MatTableDataSource();
    this._dataSource.sortingDataAccessor = (item: IEntry, property: string) => {
      if (property === 'ID') {
        return item.key;
      } else {
        return item.value[property];
      }
    };
  }

  ngAfterViewInit(): void {
    switch (this.extensionType) {
      case EJSLExtensionType.Module:
        this.css.apply(this.module, this.elem);
        break;
      case EJSLExtensionType.Component:
        this.css.apply(this.section.extension, this.elem);
        break;
    }
  }

  get dataSource(): MatTableDataSource<IEntry> {
    this._dataSource.data = this.search.getFilteredData(this._sortedData);
    return this._dataSource;
  }

  get loading(): boolean {
    return this._loading;
  }

  get displayedColumns(): string[] {
    const columns: string[] = [];
    columns.push('actions');
    this.page.getTableColumns()
      .map((a) => a.name)
      .forEach((c) => {
        columns.push(c);
      });
    columns.push('ID');
    return columns;
  }

  get extensionType(): EJSLExtensionType {
    if (!isNullOrUndefined(this.section)) {
      return EJSLExtensionType.Component;
    } else if (!isNullOrUndefined(this.module)) {
      return EJSLExtensionType.Module;
    } else {
      return null;
    }
  }

  ngOnChanges(): void {
    this.selectCollection = [];
    this.loadData();
  }

  edit() {
    if (this.selectCollection.length === 1) {
      const element = this._data.find((elemnt) => elemnt.key === this.selectCollection[0]);
      const targetPage = this.findAppropriateDetailsPage();
      this.router.navigate(['../', targetPage.name], {relativeTo: this.activatedRoute, queryParams: {ID: element.key}});
    }
  }

  findAppropriateDetailsPage(): EJSLPage {
    if (isNullOrUndefined(this.section)) {
      return null;
    }
    const targetPage = this.section.pageRefs
      .map((pageRef) => pageRef.getPage())
      .find((page) => page.type === EJSLPageType.DetailsPage && page.getEntity().name === this.page.getEntity().name);
    return targetPage;
  }

  appropriateDetailsPageAvailable(): boolean {
    return !isNullOrUndefined(this.findAppropriateDetailsPage());
  }

  add(): void {
    const targetPage = this.findAppropriateDetailsPage();
    this.router.navigate(['../', targetPage.name], {relativeTo: this.activatedRoute});
  }

  delete(): Promise<void> {
    if (confirm(`Are you sure?`)) {
      return this.selectCollection.reduce((promiseAkku: Promise<void>, id: string, index: number, array: string[]) => {
        return promiseAkku.then(() => {
          this.firebaseService.deleteItem(this.page.getEntity(), id);
        });
      }, Promise.resolve());
    } else {
      return Promise.resolve();
    }
  }

  hasReference(attribute: EJSLAttribute): boolean {
    return !isNullOrUndefined(attribute.getReference());
  }

  getReferenceDescription(attribute: EJSLAttribute): string {
    if (this.hasReference(attribute)) {
      const ref = attribute.getReference();
      if (ref.isID) {
        return `→ ${ref.getReferencedEntity().name}`;
      } else {
        return `→ ${ref.getReferencedEntity().name}.${ref.getReferencedEntityAttribute().name}`;
      }
    } else {
      return '';
    }
  }

  getLink(attribute: EJSLAttribute): EJSLLink {
    return this.page.links.find((link) => {
      return !isNullOrUndefined(link.getLinkedAttribute()) && link.getLinkedAttribute().name === attribute.name;
    });
  }

  getQueryParams(attribute: EJSLAttribute, object: IEntry) {
    const link = this.getLink(attribute);
    const params = {};
    params[this.getLinkTargetAttribute(link).name] = object[link.getLinkedAttribute().name];
    return params;
  }

  isLink(attribute: EJSLAttribute): boolean {
    if (isNullOrUndefined(this.getLink(attribute))) {
      return false;
    }
    if (isNullOrUndefined(this.section)) {
      return false;
    }
    if (this.section.extension.type === EJSLExtensionType.Component) {
      return this.section.pageRefs
        .map((p) => p.getPage().name)
        .includes(this.getLink(attribute).getTargetPage().name);
    }
    return false;
  }

  getLinkTargetAttribute(link: EJSLLink): EJSLAttribute | null {
    const attribute = link.getLinkedAttribute();
    if (attribute) {
      const reference = attribute.getReference();
      if (reference) {
        return reference.getReferencedEntityAttribute();
      } else {
        return attribute;
      }
    }
    return null;
  }

  private loadData(): void {
    this._loading = true;
    this._data = [];
    this._entitySubscription.unsubscribe();
    this._entitySubscription = this.firebaseService.fetchEntity(this.page.getEntity())
      .subscribe(
        (val: IEntry[]) => {
          this._sortedData = this._data = val;
          this._loading = false;
        });
  }

  deletecCollectionChanged(id: string, event: Event): void {
    const checked = (event.srcElement as HTMLInputElement).checked;
    if (checked) {
      if (!this.selectCollection.includes(id)) {
        this.selectCollection.push(id);
      }
    } else {
      this.selectCollection = this.selectCollection.filter((checkID) => checkID !== id);
    }
  }

  sortData(sort: Sort) {
    if (isNullOrUndefined(sort.active) || sort.direction === '') {
      this._sortedData = this._data;
      return;
    }
    this._sortedData = this._data.slice().sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this.compare(this.dataSource.sortingDataAccessor(a, sort.active), this.dataSource.sortingDataAccessor(b, sort.active), isAsc);
    });
  }

  compare(a: any, b: any, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
}
