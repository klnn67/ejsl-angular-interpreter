import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndexPageDisplayComponent } from './index-page-display.component';

describe('PageDisplayComponent', () => {
  let component: IndexPageDisplayComponent;
  let fixture: ComponentFixture<IndexPageDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndexPageDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndexPageDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
