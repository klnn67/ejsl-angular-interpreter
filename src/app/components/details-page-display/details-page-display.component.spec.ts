import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailsPageDisplayComponent } from './details-page-display.component';

describe('DetailsPageDisplayComponent', () => {
  let component: DetailsPageDisplayComponent;
  let fixture: ComponentFixture<DetailsPageDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailsPageDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailsPageDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
