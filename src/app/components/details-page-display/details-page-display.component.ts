import {AfterViewInit, Component, ElementRef, Input, OnInit} from '@angular/core';
import {EJSLDetailsPage} from '../../../ejsl-ast/page/EJSLDetailsPage';
import {FirebaseService} from '../../services/firebase.service';
import {ActivatedRoute, Router} from '@angular/router';
import {IEntry} from '../../types/entry';
import {EJSLDataType} from '../../../ejsl-ast/types/EJSLDataType';
import {EJSLAttribute} from '../../../ejsl-ast/EJSLAttribute';
import {isNullOrUndefined} from 'util';
import {ConstraintService} from '../../services/constraint.service';
import {EJSLPageAction} from '../../../ejsl-ast/EJSLPageAction';
import {EJSLLinkType} from '../../../ejsl-ast/page/EJSLLinkType';
import {EJSLPageActionType} from '../../../ejsl-ast/types/EJSLPageActionType';
import {EJSLHtmlType} from '../../../ejsl-ast/types/EJSLHtmlType';
import {EJSLEditField} from '../../../ejsl-ast/EJSLEditField';
import {CssService} from '../../services/css.service';
import {EJSLSection} from '../../../ejsl-ast/EJSLSection';

@Component({
  selector: 'app-details-page-display',
  templateUrl: './details-page-display.component.html',
  styleUrls: ['./details-page-display.component.css'],
})
export class DetailsPageDisplayComponent implements OnInit, AfterViewInit {
  DataType = EJSLDataType;
  EJSLPageActionType = EJSLPageActionType;
  EJSLHtmlType = EJSLHtmlType;
  @Input() page: EJSLDetailsPage;
  @Input() section: EJSLSection;
  object: IEntry;
  private _referenceEntriesCache: { [id: string]: { value: string, display: string }[]; } = {};

  constructor(private firebaseService: FirebaseService,
              private activatedRoute: ActivatedRoute,
              private constraint: ConstraintService,
              private router: Router,
              private css: CssService,
              private elem: ElementRef) {
  }

  ngOnInit() {
    this.activatedRoute.queryParamMap.subscribe((queryParamMap) => {
      this.firebaseService.fetchEntityOnce(this.page.getEntity()).then((objects) => {
        if (queryParamMap.keys.length > 0) {
          this.object = objects.find((o) => {
            for (const parameter of queryParamMap.keys) {
              if (parameter === 'ID') {
                return o.key === queryParamMap.get(parameter);
              } else if (o.value[parameter] !== queryParamMap.get(parameter)) {
                return false;
              }
            }
            return true;
          });
        } else {
          this.object = null;
        }

        if (isNullOrUndefined(this.object)) {
          this.object = {
            key: null,
            value: {},
          };
        }
      });
    });
  }

  ngAfterViewInit(): void {
    this.css.apply(this.section.extension, this.elem);
  }

  async pnSaveClick() {
    const error = await this.constraint.checkConstraints(this.page.getEntity(), this.object);
    if (error) {
      alert(error);
    } else {
      if (isNullOrUndefined(this.object.key)) {
        const newID = await this.firebaseService.save(this.page.getEntity(), this.object.value);
        this.router.navigate(['./'], {queryParams: {ID: newID}, relativeTo: this.activatedRoute});
      } else {
        await this.firebaseService.update(this.page.getEntity(), this.object.key, this.object.value);
      }
    }
  }

  getLabelTextForEditField(field: EJSLEditField): string {
    let text = `${field.getAttribute().name} (${EJSLDataType[field.getAttribute().type]}|${EJSLHtmlType[field.htmlType]}`;
    if (field.getAttribute().notNull) {
      text += `, Not Null`;
    }
    if (field.getAttribute().uniqueness.isUnique) {
      text += `, Unique`;
      if (field.getAttribute().uniqueness.withAttributeIndex) {
        text += ` with ${field.getAttribute().uniqueness.resolveWithAttribute().name}`;
      }
    }
    text += `)`;

    const ref = field.getAttribute().getReference();
    if (!isNullOrUndefined(ref)) {
      text += ` → ${ref.getReferencedEntity().name}`;
      if (!ref.isID) {
        text += `.${ref.getReferencedEntityAttribute().name}`;
      }
    }

    return text;
  }

  getLabelTextForColumn(column: EJSLAttribute): string {
    let text = `${column.name} (${EJSLDataType[column.type]}`;
    if (column.notNull) {
      text += `, Not Null`;
    }
    if (column.uniqueness.isUnique) {
      text += `, Unique`;
      if (column.uniqueness.withAttributeIndex) {
        text += ` with ${column.uniqueness.resolveWithAttribute().name}`;
      }
    }
    text += `)`;

    const ref = column.getReference();
    if (!isNullOrUndefined(ref)) {
      text += ` → ${ref.getReferencedEntity().name}`;
      if (!ref.isID) {
        text += `.${ref.getReferencedEntityAttribute().name}`;
      }
    }

    return text;
  }

  getReferenceEntries(column: EJSLAttribute): { value: string, display: string }[] {
    if (!this._referenceEntriesCache[column.name]) {
      const ref = column.getReference();
      const referencedEntity = ref.getReferencedEntity();
      this.firebaseService.fetchEntityOnce(referencedEntity).then((val) => {
        this._referenceEntriesCache[ref.getEntityAttribute().name] = val.map((entry) => {
          if (ref.isID) {
            return {
              value: entry.key,
              display: this.objectToDisplayString(entry.value),
            };
          } else {
            const e = entry.value[ref.getReferencedEntityAttribute().name];
            return {
              value: e,
              display: e,
            };
          }
        });
      });
    }
    return this._referenceEntriesCache[column.name];
  }

  private objectToDisplayString(object: any): string {
    let strin = '';
    Object.keys(object).forEach((key) => {
      strin += object[key] + ' ';
    });
    strin.trim();
    return strin;
  }

  getLinkedActions(): EJSLPageAction[] {
    return this.page.pageActions.filter((action) => !isNullOrUndefined(action.getLink()));
  }
}
