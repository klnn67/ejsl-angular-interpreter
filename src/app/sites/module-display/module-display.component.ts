import { Component, OnInit } from '@angular/core';
import {FirebaseService} from '../../services/firebase.service';
import {ActivatedRoute} from '@angular/router';
import {EJSLModule} from '../../../ejsl-ast/extension/EJSLModule';
import {EJSLPage} from '../../../ejsl-ast/page/EJSLPage';
import {EJSLExtensionType} from '../../../ejsl-ast/types/EJSLExtensionType';

@Component({
  selector: 'app-modul-display',
  templateUrl: './module-display.component.html',
  styleUrls: ['./module-display.component.css']
})
export class ModuleDisplayComponent implements OnInit {
  _EJSLExtensionType = EJSLExtensionType;

  module: EJSLModule;

  constructor(private firebase: FirebaseService, private activatedRoute: ActivatedRoute) {
    this.firebase.getModel(this.activatedRoute.snapshot.paramMap.get('model')).then((model) => {
      const parts = model.parts;
      this.module = model.parts[0].extensions.find((e) => e.name === this.activatedRoute.snapshot.paramMap.get('module')) as EJSLModule;
    });
  }

  ngOnInit() {
  }

  get page(): EJSLPage {
    return this.module.pageRef.getPage();
  }
}
