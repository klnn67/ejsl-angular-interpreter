import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchPluginDisplayComponent } from './search-plugin-display.component';

describe('SearchPluginDisplayComponent', () => {
  let component: SearchPluginDisplayComponent;
  let fixture: ComponentFixture<SearchPluginDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchPluginDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchPluginDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
