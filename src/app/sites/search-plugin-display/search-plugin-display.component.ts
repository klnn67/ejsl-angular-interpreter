import { Component, OnInit } from '@angular/core';
import {EJSLSearchPlugin} from '../../../ejsl-ast/extension/EJSLSearchPlugin';
import {FirebaseService} from '../../services/firebase.service';
import {ActivatedRoute} from '@angular/router';
import {EJSLPlugin} from '../../../ejsl-ast/extension/EJSLPlugin';
import {isNullOrUndefined} from 'util';
import {EJSLHtmlType} from '../../../ejsl-ast/types/EJSLHtmlType';
import {EJSLLocalParameter} from '../../../ejsl-ast/EJSLLocalParameter';
import {Search} from '../../helper/Search';
import {Subscription} from 'rxjs';
import {IEntry} from '../../types/entry';
import {MatTableDataSource, Sort} from '@angular/material';
import {EJSLAttribute} from '../../../ejsl-ast/EJSLAttribute';
import {LangService} from '../../services/lang.service';

@Component({
  selector: 'app-search-plugin-display',
  templateUrl: './search-plugin-display.component.html',
  styleUrls: ['./search-plugin-display.component.css']
})
export class SearchPluginDisplayComponent {
  _isNullOrUndefined = isNullOrUndefined;
  _EJSLHtmlType = EJSLHtmlType;

  searchPlugin: EJSLSearchPlugin;
  loading: boolean;
  parameterValues: {[key: string]: string};

  search: Search;
  private _entitySubscription: Subscription = new Subscription();
  private _data: IEntry[];
  private _sortedData: IEntry[];
  private _dataSource: MatTableDataSource<IEntry>;

  constructor(private firebase: FirebaseService,
              private activatedRoute: ActivatedRoute,
              private firebaseService: FirebaseService,
              public lang: LangService) {
    this.loading = true;
    this.firebase.getModel(this.activatedRoute.snapshot.paramMap.get('model')).then((model) => {
      const parts = model.parts;
      const plugin = model.parts[0].extensions.find((e) => e.name === this.activatedRoute.snapshot.paramMap.get('plugin')) as EJSLPlugin;
      this.searchPlugin = plugin as EJSLSearchPlugin;
      this.parameterValues = {};
      this.searchPlugin.localParameters.forEach((parameter) => {
        this.parameterValues[parameter.name] = lang.get(parameter.defaultvalue, this.searchPlugin);
      });
      this.loading = false;
    });

    this.search = new Search();
    this._data = [];
    this._sortedData = [];
    this._dataSource = new MatTableDataSource();
    this._dataSource.sortingDataAccessor = (item: IEntry, property: string) => {
      if (property === 'ID') {
        return item.key;
      } else {
        return item.value[property];
      }
    };
  }

  performSearch() {
    this.search.searchTerm = Object.values(this.parameterValues).join('|');
    this._data = [];
    this.firebaseService.fetchEntity(this.searchPlugin.getEntity())
      .subscribe(
        (val: IEntry[]) => {
          this._data = this._sortedData = this.search.getFilteredData(val);
          this.loading = false;
        });
  }

  get dataSource(): MatTableDataSource<IEntry> {
    this._dataSource.data = this.search.getFilteredData(this._sortedData);
    return this._dataSource;
  }
  get displayedColumns(): string[] {
    const columns: string[] = [];
    this.searchPlugin.getEntity().attributes
      .map((a) => a.name)
      .forEach((c) => {
        columns.push(c);
      });
    columns.push('ID');
    return columns;
  }

  hasReference(attribute: EJSLAttribute): boolean {
    return !isNullOrUndefined(attribute.getReference());
  }

  getReferenceDescription(attribute: EJSLAttribute): string {
    if (this.hasReference(attribute)) {
      const ref = attribute.getReference();
      if (ref.isID) {
        return `→ ${ref.getReferencedEntity().name}`;
      } else {
        return `→ ${ref.getReferencedEntity().name}.${ref.getReferencedEntityAttribute().name}`;
      }
    } else {
      return '';
    }
  }

  getLabelTextForEditField(parameter: EJSLLocalParameter) {
    return `${this.lang.get(parameter.label, this.searchPlugin)} (${this.lang.get(parameter.descripton, this.searchPlugin)})`;
  }


  sortData(sort: Sort) {
    if (isNullOrUndefined(sort.active) || sort.direction === '') {
      this._sortedData = this._data;
      return;
    }
    this._sortedData = this._data.slice().sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      return this.compare(this.dataSource.sortingDataAccessor(a, sort.active), this.dataSource.sortingDataAccessor(b, sort.active), isAsc);
    });
  }

  compare(a: any, b: any, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }
}
