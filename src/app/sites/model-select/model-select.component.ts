import {Component, OnInit} from '@angular/core';
import {MatDialog} from '@angular/material';
import {FirebaseService} from '../../services/firebase.service';
import {EJSLModel} from '../../../ejsl-ast/EJSLModel';
import {EJSLPageType} from '../../../ejsl-ast/types/EJSLPageType';
import {EJSLSectionType} from '../../../ejsl-ast/types/EJSLSectionType';
import {EJSLExtensionType} from '../../../ejsl-ast/types/EJSLExtensionType';
import {EJSLPage} from '../../../ejsl-ast/page/EJSLPage';
import {EJSLIndexPage} from '../../../ejsl-ast/page/EJSLIndexPage';
import {EJSLExtension} from '../../../ejsl-ast/extension/EJSLExtension';
import {EJSLComponent} from '../../../ejsl-ast/extension/EJSLComponent';
import {EJSLModule} from '../../../ejsl-ast/extension/EJSLModule';
import {AddModelModalComponent} from '../../components/add-model-modal/add-model-modal.component';
import {EJSLPluginType} from '../../../ejsl-ast/types/EJSLPluginType';
import {EJSLPlugin} from '../../../ejsl-ast/extension/EJSLPlugin';

@Component({
  selector: 'app-model-select',
  templateUrl: './model-select.component.html',
  styleUrls: ['./model-select.component.css']
})
export class ModelSelectComponent implements OnInit {

  models: EJSLModel[];
  _EJSLExtensionType = EJSLExtensionType;
  _EJSLSectionType = EJSLSectionType;
  _EJSLPageType = EJSLPageType;
  _EJSLPluginType = EJSLPluginType;
  loading: boolean;

  constructor(public dialog: MatDialog, public firebaseService: FirebaseService) {
    this.models = [];
  }

  async ngOnInit() {
    this.loading = true;
    this.firebaseService.getModels().subscribe(async (modelStrings) => {
      this.models = [];
      for (const modelString of modelStrings) {
        try {
          this.models.push(new EJSLModel(await this.firebaseService.parseStringAsnyc(modelString.payload.val()), modelString.key));
        } catch (e) {
          console.error(`Error parsing ${modelString.key}: ${e}`);
        }
      }
      this.models.sort((a, b) => a.name.localeCompare(b.name));
      this.loading = false;
    });
  }

  getIndexPages(model: EJSLModel): EJSLPage[] {
    const _entities: EJSLPage[] = [];
    model.parts.forEach((part) => {
      part.pages.forEach((page) => {
        if (page.constructor.name ===  EJSLIndexPage.name) {
          _entities.push(page);
        }
      });
    });

    return _entities;
  }

  getAuthorStringList(extension: EJSLExtension): string {
    return extension.manifest.authors.map((a) => `${a.name} (${a.authoremail})`).join(', ');
  }

  getAsComponent(extension: EJSLExtension): EJSLComponent {
    return extension as EJSLComponent;
  }

  getAsModules(extension: EJSLExtension): EJSLModule {
    return extension as EJSLModule;
  }

  getAsPlugin(extension: EJSLExtension): EJSLPlugin {
    return extension as EJSLPlugin;
  }

  getLanguagStringList(extension: EJSLExtension): string {
    return extension.languages.map((l) => `${l.name}`).join(', ');
  }

  addButtonClicked(): void {
    this.dialog.open(AddModelModalComponent);
  }

  deleteClicked(model: EJSLModel) {
    if (confirm(`Are you sure?`)) {
      this.firebaseService.deleteModel(model.filename);
    }
  }

  getSectionName(section: EJSLSectionType): string {
    switch (section) {
      case EJSLSectionType.BackendSection:
        return `Backend`;
      case EJSLSectionType.FrontendSection:
        return `Frontend`;
    }
  }
}
