import {Component, OnInit} from '@angular/core';
import {FirebaseService} from '../../services/firebase.service';
import {ActivatedRoute, Router} from '@angular/router';
import {EJSLComponent} from '../../../ejsl-ast/extension/EJSLComponent';
import {EJSLPageType} from '../../../ejsl-ast/types/EJSLPageType';
import {EJSLSection} from '../../../ejsl-ast/EJSLSection';
import {EJSLSectionType} from '../../../ejsl-ast/types/EJSLSectionType';
import {EJSLExtensionType} from '../../../ejsl-ast/types/EJSLExtensionType';
import {EJSLPage} from '../../../ejsl-ast/page/EJSLPage';

@Component({
  selector: 'app-section-display',
  templateUrl: './section-display.component.html',
  styleUrls: ['./section-display.component.css']
})
export class SectionDisplayComponent implements OnInit {
  _EJSLSectionType = EJSLSectionType;
  _EJSLExtensionType = EJSLExtensionType;
  _EJSLPageType = EJSLPageType;

  pages: EJSLPage[];
  section: EJSLSection;
  selectePage: EJSLPage;

  constructor(private firebase: FirebaseService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.pages = [];
  }

  ngOnInit() {
    this.firebase.getModel(this.activatedRoute.snapshot.paramMap.get('model')).then((model) => {
      const parts = model.parts;
      const component = model.parts[0].extensions
        .find((e) => e.name === this.activatedRoute.snapshot.paramMap.get('component')) as EJSLComponent;
      this.section = component.sections.find((s) => EJSLSectionType[s.type] === this.activatedRoute.snapshot.paramMap.get('section'));
      this.pages = this.section.pageRefs.map((r) => r.getPage());
      this.activatedRoute.paramMap.subscribe((params) => {
        const page = params.get('page');
        if (page) {
          this.selectePage = this.pages.find((p) => p.name === page);
        } else {
          const indexPages = this.pages.filter((p) => p.type === EJSLPageType.IndexPage);
          if (indexPages.length > 0) {
            this.router.navigate([`./`, indexPages[0].name], {relativeTo: this.activatedRoute});
          }
        }
      });
    });
  }

  get indexPages(): EJSLPage[] {
    return this.pages.filter((page) => page.type === EJSLPageType.IndexPage);
  }

}
