import {IEntryValue} from './entryvalue';

export interface IEntry {
  key: string;
  value: IEntryValue;
}
