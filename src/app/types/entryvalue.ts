export interface IEntryValue {
  [key: string]: any;
}
