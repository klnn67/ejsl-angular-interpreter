import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MaterialModule} from './modules/material.module';
import { LayoutModule } from '@angular/cdk/layout';
import {RouterModule} from '@angular/router';
import {appRoutes} from './routes';
import { NotFoundComponent } from './sites/not-found/not-found.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {firebase} from '../config';
import { ModelSelectComponent } from './sites/model-select/model-select.component';
import { TopbarComponent } from './components/topbar/topbar.component';
import { AddModelModalComponent } from './components/add-model-modal/add-model-modal.component';
import { SectionDisplayComponent } from './sites/section-display/section-display.component';
import { IndexPageDisplayComponent } from './components/index-page-display/index-page-display.component';
import { ModuleDisplayComponent } from './sites/module-display/module-display.component';
import { DetailsPageDisplayComponent } from './components/details-page-display/details-page-display.component';
import { SearchPluginDisplayComponent } from './sites/search-plugin-display/search-plugin-display.component';

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    ModelSelectComponent,
    TopbarComponent,
    AddModelModalComponent,
    SectionDisplayComponent,
    IndexPageDisplayComponent,
    ModuleDisplayComponent,
    DetailsPageDisplayComponent,
    SearchPluginDisplayComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    LayoutModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(firebase),
    AngularFireDatabaseModule,
  ],
  providers: [],
  entryComponents: [
    AddModelModalComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
