import {ElementRef, Injectable} from '@angular/core';
import {EJSLExtension} from '../../ejsl-ast/extension/EJSLExtension';
import {EJSLExtensionType} from '../../ejsl-ast/types/EJSLExtensionType';
import {EJSLTemplate} from '../../ejsl-ast/extension/EJSLTemplate';
import {isNullOrUndefined} from 'util';
import {EJSLCssBlock} from '../../ejsl-ast/template/EJSLCssBlock';
import {EJSLTemplatePosition} from '../../ejsl-ast/template/EJSLTemplatePosition';
import {EJSLPositionType} from '../../ejsl-ast/types/EJSLPositionType';
import {EJSLKeyValuePair} from '../../ejsl-ast/EJSLKeyValuePair';
import {EJSLPositionExtensionType} from '../../ejsl-ast/types/EJSLPositionExtensionType';

@Injectable({
  providedIn: 'root'
})
export class CssService {

  constructor() { }

  private static keyValueTtoCSS(keyValue: EJSLKeyValuePair): string {
    return `\t${keyValue.name}: ${keyValue.value};`;
  }

  private static blockToCSS(block: EJSLCssBlock): string {
    const statements = block.keyvaluepairs.map((kv) => this.keyValueTtoCSS(kv)).join('\n');
    return `${block.selector} {\n${statements}\n}\n`;
  }

  private static positionToCSS(position: EJSLTemplatePosition, extension: EJSLExtension): string {
    const container = (() => {
      if (position.name === EJSLPositionType.head) {
        return `mat-toolbar`;
      } else {
        throw new Error(`Unsupported Positiontype: ${position.name}. Cannot create CSS`);
      }
    })();

    const blocks = position.parameters
      .filter((para) => {
        // filter parameters that only apply to a certain extension type
        if (extension.type === EJSLExtensionType.Component) {
          return para.type === EJSLPositionExtensionType.component;
        } else if (extension.type === EJSLExtensionType.Module) {
          return para.type === EJSLPositionExtensionType.modules;
        }
        return false;
      })
      .map((para) => {
      const statements = para.keyvaluepairs.map((kv) => this.keyValueTtoCSS(kv)).join('\n');
      return `// ${para.name}\n${container} > #${para.divid} {\n${statements}\n}\n`;
    }).join('\n');

    return blocks;
  }

  apply(extension: EJSLExtension, element: ElementRef) {
    const template = extension.part.extensions.find((ext) => ext.type === EJSLExtensionType.Template) as EJSLTemplate;
    if (isNullOrUndefined(template)) {
      return;
    }

    const head = document.getElementsByTagName(element.nativeElement.tagName)[0];
    const s = document.createElement('style');
    s.setAttribute('type', 'text/css');
    const globalCSS = template.cssBlocks.map((b) => CssService.blockToCSS(b)).join('\n');
    const positionalCSS = template.positions.map((p) => CssService.positionToCSS(p, extension)).join('\n');
    s.appendChild(document.createTextNode(`\n${globalCSS}\n${positionalCSS}`));
    head.appendChild(s);
  }
}
