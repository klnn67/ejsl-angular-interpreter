import {Injectable} from '@angular/core';
import {AngularFireDatabase, SnapshotAction} from 'angularfire2/database';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {IEntry} from '../types/entry';
import {IEntryValue} from '../types/entryvalue';
import {EJSLEntity} from '../../ejsl-ast/EJSLEntity';
import {EJSLModel} from '../../ejsl-ast/EJSLModel';
import * as xml2js from 'xml2js';
import {MatSnackBar} from '@angular/material';
import Reference = firebase.database.Reference;

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {

  constructor(private db: AngularFireDatabase, private snackBar: MatSnackBar) {
  }

  save(entity: EJSLEntity, data: IEntryValue): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      return this.db.list(this.dataBaseRef(entity)).push(data).then((info: Reference) => {
        this.snackBar.open(`${entity.name} saved.`);
        resolve(info.key);
      }, (err) => {
        this.snackBar.open(err);
        reject(err);
      });
    });
  }

  fetchEntity(entity: EJSLEntity): Observable<IEntry[]> {
    const path: string = this.dataBaseRef(entity);
    return this.db.list(path).snapshotChanges().pipe(map((c) => c.map(action => {
      return {
        key: action.key,
        value: action.payload.val(),
      };
    })));
  }

  fetchEntityOnce(entity: EJSLEntity): Promise<IEntry[]> {
    return new Promise<IEntry[]>((resolve, reject) => {
      const sub = this.fetchEntity(entity).subscribe((val) => {
        resolve(val);
        sub.unsubscribe();
      });
    });
  }

  async deleteItem(entity: EJSLEntity, key: string) {
    try {
      await this.db.list(this.dataBaseRef(entity)).remove(key);
      this.snackBar.open(`${entity.name} removed.`);
    } catch (err) {
      this.snackBar.open(err);
    }
  }

  async update(entity: EJSLEntity, key: string, value: IEntryValue) {
    try {
      await this.db.list(this.dataBaseRef(entity)).set(key, Object.assign({}, value));
      this.snackBar.open(`${entity.name} updated.`);
    } catch (err) {
      this.snackBar.open(err);
    }
  }

  async saveModel(modelXML: string, title: string): Promise<void> {
    try {
      await this.db.list(this.modelBaseHref()).push(modelXML);
      this.snackBar.open(`${title} saved.`);
    } catch (err) {
      this.snackBar.open(err);
    }
  }

  async deleteModel(modelID: string) {
    try {
      await this.db.list(this.modelBaseHref()).remove(modelID);
      this.snackBar.open(`${modelID} removed.`);
    } catch (err) {
      this.snackBar.open(err);
    }
  }

  getModels(): Observable<SnapshotAction<any>[]> {
    return this.db.list(this.modelBaseHref()).snapshotChanges();
  }

  getModel(id: string): Promise<EJSLModel> {
    return new Promise<EJSLModel>((resolve, reject) => {
      const sub = this.db.object(`${this.modelBaseHref()}/${id}`).snapshotChanges().subscribe((s) => {
        this.parseStringAsnyc(s.payload.val())
          .then((xmi) => {
            try {
              const ejslModel = new EJSLModel(xmi, s.key);
              sub.unsubscribe();
              resolve(ejslModel);
            } catch (err) {
              reject(new Error(`Error parsing model ${id}: ${err}`));
            }
          })
          .catch((err) => reject(err));
      });
    });
  }

  async parseStringAsnyc(xmiString): Promise<any> {
    return new Promise((resolve, reject) => {
      xml2js.parseString(xmiString, function (err, result) {
        if (err) {
          reject(err);
        } else {
          resolve(result);
        }
      });
    });
  }

  private dataBaseRef(entity: EJSLEntity): string {
    return `data/${entity.part.model.name}/${entity.part.name}/${entity.name}`;
  }

  private modelBaseHref(): string {
    return `models`;
  }
}
