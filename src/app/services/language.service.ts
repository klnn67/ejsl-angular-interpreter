import { Injectable } from '@angular/core';

/* tslint:disable:max-line-length */

@Injectable({
  providedIn: 'root'
})
export class LanguageService {

  constructor() { }

  getSimpleUniqueness(attribute: string, conflictingObject: string): string {
    return `The column '${attribute}' is violating Uniqeness of the current object.
Input another value for '${attribute}'.

The violated object:
${conflictingObject}`;
  }

  getCompoundUniqeness(attribute: string, extraAttribute: string, conflictingObject: string): string {
    return `The columns '${attribute}' and '${extraAttribute}' are violating Uniqeness of the current object, since theese columns are compound unique.
Input another value for '${attribute}' or '${extraAttribute}'.

The violated object:
${conflictingObject}`;
  }

  getNotNull(attribute: string): string {
    return `The attribute ${attribute} can not be null.`;
  }

  getNoRefrencedObject(attribute: string, referencedEntity: string): string {
    return `No ${referencedEntity} was referenced with ${attribute}.`;
  }
}
