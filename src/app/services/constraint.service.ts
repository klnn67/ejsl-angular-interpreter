import {Injectable} from '@angular/core';
import {FirebaseService} from './firebase.service';
import {IEntry} from '../types/entry';
import {LanguageService} from './language.service';
import {isNullOrUndefined, isString} from 'util';
import {EJSLPart} from '../../ejsl-ast/EJSLPart';
import {EJSLModel} from '../../ejsl-ast/EJSLModel';
import {EJSLEntity} from '../../ejsl-ast/EJSLEntity';
import {EJSLAttribute} from '../../ejsl-ast/EJSLAttribute';

@Injectable({
  providedIn: 'root'
})
export class ConstraintService {

  constructor(private firebase: FirebaseService,
              private language: LanguageService) { }

  async checkConstraints(entity: EJSLEntity, object: IEntry): Promise<string | null> {
        for (const attribute of entity.attributes) {

      // 1: Check Uniqueness
      const uniqueness = attribute.uniqueness;
      if (uniqueness.isUnique) {
        if (!isNullOrUndefined(uniqueness.withAttributeIndex)) {
          if (uniqueness.isID) {
            // Do nothing, because ID is always unique
          } else {
            // Check uniqueness with a compound unique key.
            const extraAttribute = uniqueness.resolveWithAttribute();
            const conflictingObject = await this.checkUniquenessWithExtraAttribute(
              entity,
              object,
              attribute,
              extraAttribute);

            if (conflictingObject) {
              return this.language.getCompoundUniqeness(attribute.name, extraAttribute.name, JSON.stringify(conflictingObject, null, 2));
            }
          }
        } else {
          // Check the uniqueness without any extra attributes.
          const conflictingObject = await this.checkSimpleUniqueness(object, attribute);
          if (conflictingObject) {
            return this.language.getSimpleUniqueness(attribute.name, JSON.stringify(conflictingObject, null, 2));
          }
        }
      }

      // 2: Not null
      if (attribute.notNull) {
        if (isNullOrUndefined(object.value[attribute.name])) {
          return this.language.getNotNull(attribute.name);
        }
        if (isString(object.value[attribute.name]) && (object.value[attribute.name] as string).length === 0) {
          return this.language.getNotNull(attribute.name);
        }
      }

      // 3: References
      const ref = attribute.getReference();
      if (ref && !ref.isID) {
        if (ref.min === 0 && object.value[ref.getEntityAttribute().name] === '') {
          // allow
        } else {
          const referencedEntityObjects: IEntry[] = await this.firebase.fetchEntityOnce(ref.getReferencedEntity());
          const referencedObjects = referencedEntityObjects.filter((obj) => {
            return obj.value[ref.getReferencedEntityAttribute().name] === object.value[ref.getEntityAttribute().name];
          });
          if (referencedObjects.length === 0) {
            return this.language.getNoRefrencedObject(ref.getEntityAttribute().name, ref.getReferencedEntity().name);
          }
        }
      }
    }
    return null;
  }

  private async checkSimpleUniqueness(
    object: IEntry,
    attribute: EJSLAttribute): Promise<IEntry> {
    const data = await this.firebase.fetchEntityOnce(attribute.entity);
    const conflictingObject = data.find((obj) => {
      if (object.key === obj.key) {
        return false;
      }
      return (obj.value[attribute.name] === object.value[attribute.name])
        || (obj.value[attribute.name] === '' && isNullOrUndefined(object.value[attribute.name]))
        || (isNullOrUndefined(obj.value[attribute.name]) && object.value[attribute.name] === '');
    });
    if (conflictingObject) {
      return conflictingObject;
    } else {
      return null;
    }
  }

  private async checkUniquenessWithExtraAttribute(
    entity: EJSLEntity,
    object: IEntry,
    attribute: EJSLAttribute,
    extraAttribute: EJSLAttribute): Promise<IEntry> {
    const data = await this.firebase.fetchEntityOnce(entity);
    const conflictingObject = data.find((obj) => {
      if (object.key === obj.key) {
        return false;
      }
      const mainAttributeViolation = (obj.value[attribute.name] === object.value[attribute.name])
        || (obj.value[attribute.name] === '' && isNullOrUndefined(object.value[attribute.name]))
        || (isNullOrUndefined(obj.value[attribute.name]) && object.value[attribute.name] === '');
      const withAttributeVoilation = (obj.value[extraAttribute.name] === object.value[extraAttribute.name])
        || (obj.value[extraAttribute.name] === '' && isNullOrUndefined(object.value[extraAttribute.name]))
        || (isNullOrUndefined(obj.value[extraAttribute.name]) && object.value[extraAttribute.name] === '');

      return mainAttributeViolation && withAttributeVoilation;
    });
    if (conflictingObject) {
      return conflictingObject;
    } else {
      return null;
    }
  }
}
