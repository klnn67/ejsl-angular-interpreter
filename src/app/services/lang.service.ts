import { Injectable } from '@angular/core';
import {EJSLExtension} from '../../ejsl-ast/extension/EJSLExtension';
import {isNullOrUndefined} from 'util';

@Injectable({
  providedIn: 'root'
})
export class LangService {

  langToUse = 'de-DE';

  constructor() { }

  get(key: string, extentsion: EJSLExtension): string {
    const language = extentsion.languages.find((l) => l.name === this.langToUse);
    if (isNullOrUndefined(language)) {
      return key;
    }

    const keyValuePair = language.keyvaluepairs.find((kv) => kv.name === key);
    if (isNullOrUndefined(keyValuePair)) {
      return key;
    }

    return keyValuePair.value;
  }
}
