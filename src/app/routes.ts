import {Routes} from '@angular/router';
import {NotFoundComponent} from './sites/not-found/not-found.component';
import {ModelSelectComponent} from './sites/model-select/model-select.component';
import {SectionDisplayComponent} from './sites/section-display/section-display.component';
import {ModuleDisplayComponent} from './sites/module-display/module-display.component';
import {SearchPluginDisplayComponent} from './sites/search-plugin-display/search-plugin-display.component';

export const appRoutes: Routes = [
  { path: '', component: ModelSelectComponent },
  { path: ':model/component/:component/:section', component: SectionDisplayComponent },
  { path: ':model/component/:component/:section/:page', component: SectionDisplayComponent },
  { path: ':model/module/:module', component: ModuleDisplayComponent },
  { path: ':model/search/:plugin', component: SearchPluginDisplayComponent },
  { path: '**', component: NotFoundComponent},
];
