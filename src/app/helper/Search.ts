import {isNullOrUndefined} from 'util';
import {IEntry} from '../types/entry';

export class Search {

  searchTerm: string;

  constructor() {
    this.searchTerm = '';
  }

  private filterEntry(entry: IEntry): boolean {
    if ((entry.value as any).toString().toLowerCase().includes(this.searchTerm.toLowerCase())) {
      return true;
    }

    if ((entry.key as any).toString().toLowerCase().includes(this.searchTerm.toLowerCase())) {
      return true;
    }

    return Object.keys(entry.value).reduce((akku: boolean, column: string, index: number, array: string[]) => {
      const rawField = entry.value[column];
      const field = isNullOrUndefined(rawField) ? '' :  `${rawField}`;

      if (field.trim().length === 0) {
        return akku;
      }

      const predicate1 = `${field}`.toLowerCase().includes(this.searchTerm.toLowerCase());
      const predicate2 = new RegExp(this.searchTerm).test(`${field}`);
      return akku || predicate1 || predicate2;
    }, false);
  }

  public getFilteredData(data: IEntry[]): IEntry[] {
    return data.filter((row: IEntry) => {
      return this.filterEntry(row);
    });
  }
}
