import {EJSLExtension} from './extension/EJSLExtension';
import {EJSLAuthor} from './EJSLAuthor';

export class EJSLManifest {
  extension: EJSLExtension;
  copyright: string;
  description: string;
  license: string;
  version: string;
  authors: EJSLAuthor[];

  constructor(xmi: any, extension: EJSLExtension) {
    this.extension = extension;

    if (xmi.$) {
      this.copyright = xmi.$.copyright;
      this.description = xmi.$.description;
      this.license = xmi.$.license;
      this.version = xmi.$.version;
    }

    const authors = xmi.authors as any[];
    if (authors) {
      this.authors = authors.map((a) => new EJSLAuthor(a, this));
    } else {
      this.authors = [];
    }
  }
}
