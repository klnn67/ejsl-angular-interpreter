import {EJSLAttribute} from './EJSLAttribute';
import {EJSLReference} from './EJSLReference';
import {EJSLPart} from './EJSLPart';
import {isNullOrUndefined} from 'util';

export class EJSLEntity {
  part: EJSLPart;
  name: string;
  attributes: EJSLAttribute[];
  references: EJSLReference[];

  constructor(xmi: any, part: EJSLPart) {
    this.part = part;
    this.name = xmi.$.name;

    if (!isNullOrUndefined(xmi.attributes)) {
      this.attributes = (xmi.attributes as any[]).map((a) => new EJSLAttribute(a, this));
    } else {
      this.attributes = [];
    }

    if (!isNullOrUndefined(xmi.references)) {
      this.references = (xmi.references as any[]).map((r) => new EJSLReference(r, this));
    } else {
      this.references = [];
    }
  }
}
