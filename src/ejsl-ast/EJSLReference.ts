import {EJSLAttribute} from './EJSLAttribute';
import {EJSLEntity} from './EJSLEntity';
import {EJSLPart} from './EJSLPart';

export class EJSLReference {
  entity: EJSLEntity;
  entityAttributeIndex: number;
  referencedEntityIndex: number;
  referencedEntityAttributeIndex: string | null;
  isID: boolean;
  min: number;
  max: number;

  constructor(ref: any, entity: EJSLEntity) {
    this.entity = entity;

    const attributeIndexString = (ref.$.attribute as string).split('.')[2];
    const attributeIndex = parseInt(attributeIndexString, 10);

    const referencedEntityIndexString = (ref.$.entity as string).split('.')[1];
    const referencedEntityIndex = parseInt(referencedEntityIndexString, 10);

    const isID = ref.$.id === 'true';

    let referencedEntityAttributeIndex;
    if (!isID) {
      const referencedEntityAttributeIndexString = (ref.$.attributerefereced as string).split('.')[2];
      referencedEntityAttributeIndex = parseInt(referencedEntityAttributeIndexString, 10);
    }

    const min = parseInt(ref.$.lower, 10);
    const max = parseInt(ref.$.upper, 10);

    this.entityAttributeIndex = attributeIndex;
    this.referencedEntityIndex = referencedEntityIndex;
    this.referencedEntityAttributeIndex = referencedEntityAttributeIndex;
    this.isID = isID;
    this.min = min;
    this.max = max;
  }

  getEntityAttribute(): EJSLAttribute {
    return this.entity.attributes[this.entityAttributeIndex];
  }

  getReferencedEntity(): EJSLEntity {
    return this.entity.part.entities[this.referencedEntityIndex];
  }

  getReferencedEntityAttribute(): EJSLAttribute {
    return this.getReferencedEntity().attributes[this.referencedEntityAttributeIndex];
  }
}
