import {EJSLPage} from './page/EJSLPage';
import {EJSLPageActionType} from './types/EJSLPageActionType';
import {EJSLLink} from './link/EJSLLink';
import {isNullOrUndefined} from 'util';

export class EJSLPageAction {
  page: EJSLPage;
  name: string;
  type: EJSLPageActionType;

  constructor(xmi: any, page: EJSLPage) {
    this.page = page;
    this.name = xmi.$.name;
    this.type = EJSLPageAction.resolveType(xmi);

    // TODO: Page action position is not part of the xmi.
    /*
     * PageAction zurueck {
     *   type = CANCEL position = top
     * }
     */
  }

  private static resolveType(xmi: any): EJSLPageActionType {
    const typeString = xmi.$.pageActionType;
    switch (typeString) {
      case 'CANCEL':
        return EJSLPageActionType.CANCEL;
      default:
        throw new Error(`Unsupported PageActionType: ${typeString}.`);
    }
  }

  getLink(): EJSLLink | null {
    return this.page.links.find((link) => {
      if (!isNullOrUndefined(link.getLinkedAction())) {
        return link.getLinkedAction().name === this.name;
      } else {
        return false;
      }
    });
  }
}
