import {EJSLEditField} from './EJSLEditField';

export class EJSLEditFieldAttribute {
  attribute: EJSLEditField;
  name: string;
  value: string;

  constructor(xmi: any, attribute: EJSLEditField) {
    this.attribute = attribute;
    this.name = xmi.$.name;
    this.value = xmi.$.value;
  }
}
