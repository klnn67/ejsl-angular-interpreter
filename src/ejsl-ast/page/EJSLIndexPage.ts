import {EJSLPage} from './EJSLPage';
import {EJSLPart} from '../EJSLPart';
import {EJSLLink} from '../link/EJSLLink';
import {EJSLAttribute} from '../EJSLAttribute';

export class EJSLIndexPage extends EJSLPage {
  filterIndexes: number[];

  constructor(xmi: any, part: EJSLPart) {
    super(xmi, part);
    const filters = xmi.$.filters as string;
    if (filters) {
      this.filterIndexes = filters.split(' ').map((filter) => {
        return parseInt((filter as string).split('.')[2], 10);
      });
    } else {
      this.filterIndexes = [];
    }
  }

  getFilterColumns(): EJSLAttribute[] {
    return this.filterIndexes.map((index) => {
      return this.getEntity().attributes[index];
    });
  }
}
