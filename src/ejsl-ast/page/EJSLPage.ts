import {EJSLPart} from '../EJSLPart';
import {EJSLPageType} from '../types/EJSLPageType';
import {EJSLLink} from '../link/EJSLLink';
import {EJSLLinkType} from './EJSLLinkType';
import {EJSLContextLink} from '../link/EJSLContextLink';
import {EJSLInternalLink} from '../link/EJSLInternalLink';
import {EJSLEntity} from '../EJSLEntity';
import {EJSLAttribute} from '../EJSLAttribute';

export abstract class EJSLPage {
  part: EJSLPart;
  name: string;
  entityIndex: number;
  type: EJSLPageType;
  links: EJSLLink[];
  tableColumnIndexes: number[];

  constructor(xmi: any, part: EJSLPart) {
    this.part = part;
    this.name = xmi.$.name;

    this.entityIndex = parseInt((xmi.$.entities as string).split('.')[1], 10);
    this.type = EJSLPage.resolvePageType(xmi);

    const links = xmi.links as any[];
    if (links) {
      this.links = links.map((l) => {
        if (EJSLLink.resolveLinkType(l) === EJSLLinkType.ContextLink) {
          return new EJSLContextLink(l, this);
        } else if (EJSLLink.resolveLinkType(l) === EJSLLinkType.InternalLink) {
          return new EJSLInternalLink(l, this);
        }
      });
    }

    const tablecolumns = xmi.$.tablecolumns as string;
    if (tablecolumns) {
      this.tableColumnIndexes = tablecolumns.split(' ').map((tablecolumn) => {
        return parseInt((tablecolumn as string).split('.')[2], 10);
      });
    } else {
      this.tableColumnIndexes = [];
    }
  }

  static resolvePageType(xmi: any): EJSLPageType {
    const type = xmi.$['xsi:type'] as string;
    switch (type) {
      case 'eJSL:IndexPage':
        return EJSLPageType.IndexPage;
      case 'eJSL:DetailsPage':
        return EJSLPageType.DetailsPage;
      default:
        throw Error(`Unsupported PageType: ${type}`);
    }
  }

  getTableColumns(): EJSLAttribute[] | null {
    if (this.tableColumnIndexes.length === 0) {
      return this.getEntity().attributes;
    } else {
      return this.tableColumnIndexes.map((index) => {
        return this.getEntity().attributes[index];
      });
    }
  }

  getEntity(): EJSLEntity {
    return this.part.entities[this.entityIndex];
  }
}
