import {EJSLPage} from './EJSLPage';
import {EJSLLink} from '../link/EJSLLink';
import {EJSLPart} from '../EJSLPart';
import {EJSLEditField} from '../EJSLEditField';
import {EJSLPageAction} from '../EJSLPageAction';

export class EJSLDetailsPage extends EJSLPage {
  editFields: EJSLEditField[];
  pageActions: EJSLPageAction[];

  constructor(xmi: any, part: EJSLPart) {
    super(xmi, part);

    const editFields = xmi.editfields as any[];
    if (editFields) {
      this.editFields = editFields.map((e) => new EJSLEditField(e, this));
    } else {
      this.editFields = [];
    }

    const pageactions = xmi.pageactions as any[];
    if (pageactions) {
      this.pageActions = pageactions.map((e) => new EJSLPageAction(e, this));
    } else {
      this.pageActions = [];
    }
  }
}
