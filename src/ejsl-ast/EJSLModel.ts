import {EJSLPart} from './EJSLPart';

export class EJSLModel {
  filename: string;
  name: string;
  parts: EJSLPart[];

  constructor(xmi: any, filename: string) {
    this.filename = filename;
    this.name = xmi['eJSL:EJSLModel'].$.name;
    this.parts = (xmi['eJSL:EJSLModel'].ejslPart as any[]).map((p) => new EJSLPart(p, this));
  }
}
