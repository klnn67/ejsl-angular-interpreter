import {EJSLHtmlType} from './types/EJSLHtmlType';
import {EJSLSearchPlugin} from './extension/EJSLSearchPlugin';
import {EJSLExtension} from './extension/EJSLExtension';

export class EJSLLocalParameter {
  extension: EJSLExtension;
  name: string;
  defaultvalue: string;
  label: string;
  descripton: string;
  dtype: EJSLHtmlType;

  constructor(xmi: any, extension: EJSLExtension) {
    this.extension = extension;

    this.name = xmi.$['name'];
    this.defaultvalue = xmi.$['defaultvalue'];
    this.label = xmi.$['label'];
    this.descripton = xmi.$['descripton'];

    this.dtype = EJSLLocalParameter.resolveDType(xmi);
  }

  private static resolveDType(xmi: any): EJSLHtmlType {
    const htmlTypeString = xmi.dtype[0].$.htmltype as string;
    if (!htmlTypeString) {
      const typeType = xmi.htmltype[0].$['xsi:type'];
      if (typeType === 'eJSL:ComplexHTMLTypes') {
        return EJSLHtmlType.Select;
      } else if (typeType === 'eJSL:SimpleHTMLTypes') {
        return EJSLHtmlType.Integer;
      } else {
        throw new Error(`Unsupported HtmlTypeType: ${typeType}.`);
      }
    } else {
      switch (htmlTypeString) {
        case 'Integer':
          return EJSLHtmlType.Integer;
        case 'Text_Field':
          return EJSLHtmlType.Text_Field;
        case 'Editor':
          return EJSLHtmlType.Editor;
        case 'Select':
          return EJSLHtmlType.Select;
        case 'Datepicker':
          return EJSLHtmlType.Datepicker;
        default:
          throw new Error(`Unsupported HtmlType: ${htmlTypeString}.`);
      }
    }
  }
}
