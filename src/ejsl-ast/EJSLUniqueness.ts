import {EJSLEntity} from './EJSLEntity';
import {EJSLAttribute} from './EJSLAttribute';

export class EJSLUniqueness {
  attribute: EJSLAttribute;
  isUnique: boolean;
  isID: boolean;
  withAttributeIndex: number | null;

  constructor(xmi: any, attribute: EJSLAttribute) {
    this.attribute = attribute;
    this.isUnique = xmi.$.isunique === 'true';
    this.isID = xmi.$.id === 'true';
    this.withAttributeIndex = EJSLUniqueness.getWithAttributeIndexFromUniqueExpression(xmi);
  }

  private static getWithAttributeIndexFromUniqueExpression(xmi: any): number | null {
    const indexAsString = xmi.$.withattribute ? (xmi.$.withattribute as string).split('.')[2] : null;
    if (indexAsString) {
      return parseInt(indexAsString, 10);
    } else {
      return null;
    }
  }

  resolveWithAttribute(): EJSLAttribute {
    return this.attribute.entity.attributes[this.withAttributeIndex];
  }
}
