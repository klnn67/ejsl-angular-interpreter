import {EJSLModule} from './extension/EJSLModule';
import {EJSLPage} from './page/EJSLPage';
import {EJSLComponent} from './extension/EJSLComponent';

export class EJSLModulePageRef {
  module: EJSLModule;
  pageIndex: number;
  pageSrcIndex: number;

  constructor(xmi: any, module: EJSLModule) {
    this.module = module;

    this.pageIndex = parseInt((xmi.$.page as string).split('.')[1], 10);
    this.pageSrcIndex = parseInt((xmi.pagescr[0].$.ref as string).split('.')[1], 10);
  }

  getPage(): EJSLPage {
    return this.module.part.pages[this.pageIndex];
  }

  getPageSource(): EJSLComponent {
    return this.module.part.extensions[this.pageSrcIndex] as EJSLComponent;
  }
}
