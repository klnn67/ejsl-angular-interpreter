import {EJSLManifest} from './EJSLManifest';

export class EJSLAuthor {
  manifest: EJSLManifest;
  authoremail: string;
  authorurl: string;
  name: string;

  constructor(xmi: any, manifest: EJSLManifest) {
    this.manifest = manifest;
    this.authoremail = xmi.$.authoremail;
    this.authorurl = xmi.$.authorurl;
    this.name = xmi.$.name;
  }
}
