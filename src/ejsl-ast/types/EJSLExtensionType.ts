export enum EJSLExtensionType {
  Component, Module, ExtensionPackage, Plugin, Template
}
