export enum EJSLDataType {
  Integer,
  Boolean,
  Text,
  Short_Text,
  Time,
  Date,
  Datetime,
  Link,
  Image,
  File,
  Label,
  Encrypted_Text
}
