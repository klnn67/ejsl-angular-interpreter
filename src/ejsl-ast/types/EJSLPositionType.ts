export enum EJSLPositionType {
  head, contents, footer, left, right
}
