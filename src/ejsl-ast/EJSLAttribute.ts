import {EJSLDataType} from './types/EJSLDataType';
import {EJSLUniqueness} from './EJSLUniqueness';
import {EJSLPart} from './EJSLPart';
import {EJSLReference} from './EJSLReference';
import {EJSLEntity} from './EJSLEntity';

export class EJSLAttribute {
  entity: EJSLEntity;
  name: string;
  type: EJSLDataType;
  notNull: boolean;
  uniqueness: EJSLUniqueness;

  constructor(xmi: any, entity: EJSLEntity) {
    this.entity = entity;
    this.name = xmi.$.name;
    this.type = EJSLAttribute.typeStringToType(xmi.type[0].$['type']);
    this.notNull = xmi.type[0].$['notnull'] === 'true';
    this.uniqueness = new EJSLUniqueness(xmi, this);
  }

  private static typeStringToType(type: string): EJSLDataType {
    switch (type) {
      case 'Integer':
        return EJSLDataType.Integer;
      case 'Boolean':
        return EJSLDataType.Boolean;
      case 'Text':
        return EJSLDataType.Text;
      case 'Short_Text':
        return EJSLDataType.Short_Text;
      case 'Time':
        return EJSLDataType.Time;
      case 'Date':
        return EJSLDataType.Date;
      case 'Datetime':
        return EJSLDataType.Datetime;
      case 'Link':
        return EJSLDataType.Link;
      case 'Image':
        return EJSLDataType.Image;
      case 'File':
        return EJSLDataType.File;
      case 'Label':
        return EJSLDataType.Label;
      case 'Encrypted_Text':
        return EJSLDataType.Encrypted_Text;
      default:
        return EJSLDataType.Integer;
    }
  }

  getReference(): EJSLReference | null {
    return this.entity.references.find((r) => r.getEntityAttribute().name === this.name);
  }
}
