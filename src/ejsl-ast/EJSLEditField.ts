import {EJSLPage} from './page/EJSLPage';
import {EJSLHtmlType} from './types/EJSLHtmlType';
import {EJSLAttribute} from './EJSLAttribute';
import {EJSLEditFieldAttribute} from './EJSLEditFieldAttribute';
import {EJSLEditFieldValue} from './EJSLEditFieldValue';

export class EJSLEditField {
  page: EJSLPage;
  attributeIndex: number;
  htmlType: EJSLHtmlType;
  attributes: EJSLEditFieldAttribute[];
  values: EJSLEditFieldValue[];

  constructor(xmi: any, page: EJSLPage) {
    this.page = page;

    this.attributeIndex = parseInt(xmi.$.attribute.split('/')[5].split('.')[1], 10);
    this.htmlType = EJSLEditField.resolveHtmlType(xmi);

    const attributes = xmi.attributes as any[];
    if (attributes) {
      this.attributes = attributes.map((a) => new EJSLEditFieldAttribute(a, this));
    } else {
      this.attributes = [];
    }

    const values = xmi.values as any[];
    if (values) {
      this.values = values.map((a) => new EJSLEditFieldValue(a, this));
    } else {
      this.values = [];
    }
  }

  private static resolveHtmlType(xmi: any): EJSLHtmlType {
    const htmlTypeString = xmi.htmltype[0].$.htmltype as string;
    if (!htmlTypeString) {
      const typeType = xmi.htmltype[0].$['xsi:type'];
      if (typeType === 'eJSL:ComplexHTMLTypes') {
         return EJSLHtmlType.Select;
      } else if (typeType === 'eJSL:SimpleHTMLTypes') {
        return EJSLHtmlType.Integer;
      } else {
        throw new Error(`Unsupported HtmlTypeType: ${typeType}.`);
      }
    } else {
      switch (htmlTypeString) {
        case 'Integer':
          return EJSLHtmlType.Integer;
        case 'Text_Field':
          return EJSLHtmlType.Text_Field;
        case 'Editor':
          return EJSLHtmlType.Editor;
        case 'Select':
          return EJSLHtmlType.Select;
        case 'Datepicker':
          return EJSLHtmlType.Datepicker;
        default:
          throw new Error(`Unsupported HtmlType: ${htmlTypeString}.`);
      }
    }
  }

  getAttribute(): EJSLAttribute {
    return this.page.getEntity().attributes[this.attributeIndex];
  }
}
