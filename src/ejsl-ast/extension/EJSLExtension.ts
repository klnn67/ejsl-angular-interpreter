import {EJSLPart} from '../EJSLPart';
import {EJSLExtensionType} from '../types/EJSLExtensionType';
import {EJSLLanguage} from '../EJSLLanguage';
import {EJSLManifest} from '../EJSLManifest';

export abstract class EJSLExtension {
  part: EJSLPart;
  name: string;
  languages: EJSLLanguage[];
  manifest: EJSLManifest | null;
  type: EJSLExtensionType;

  constructor(xmi: any, part: EJSLPart) {
    this.part = part;
    this.name = xmi.$.name;
    this.type = EJSLExtension.resolveType(xmi);

    const languages = xmi.languages as any[];
    if (languages) {
      this.languages = languages.map((l) => new EJSLLanguage(l, this));
    } else {
      this.languages = [];
    }

    const manifest = xmi.manifest[0];
    if (manifest) {
      this.manifest = new EJSLManifest(manifest, this);
    } else {
      this.manifest = null;
    }
  }

  static resolveType(xmi: any): EJSLExtensionType {
    const typeString = xmi.$['xsi:type'];

    switch (typeString) {
      case 'eJSL:Component':
        return EJSLExtensionType.Component;
      case 'eJSL:Module':
        return EJSLExtensionType.Module;
      case 'eJSL:ExtensionPackage':
        return EJSLExtensionType.ExtensionPackage;
      case 'eJSL:Plugin':
        return EJSLExtensionType.Plugin;
      case 'eJSL:Template':
        return EJSLExtensionType.Template;
      default:
        throw new Error(`Unsupported ExtensionType: ${typeString}.`);
    }
  }
}
