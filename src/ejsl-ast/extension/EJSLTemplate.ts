import {EJSLPart} from '../EJSLPart';
import {EJSLExtension} from './EJSLExtension';
import {EJSLLocalParameter} from '../EJSLLocalParameter';
import {isNullOrUndefined} from 'util';
import {EJSLTemplatePosition} from '../template/EJSLTemplatePosition';
import {EJSLCssBlock} from '../template/EJSLCssBlock';

export class EJSLTemplate extends EJSLExtension {
  localParameters: EJSLLocalParameter[];
  positions: EJSLTemplatePosition[];
  cssBlocks: EJSLCssBlock[];

  constructor(xmi: any, part: EJSLPart) {
    super(xmi, part);

    const localParameters = xmi['localparameters'] as any[];
    if (isNullOrUndefined(localParameters)) {
      this.localParameters = [];
    } else {
      this.localParameters = localParameters.map((lp) => new EJSLLocalParameter(lp, this));
    }

    const positions = xmi['positions'] as any[];
    if (isNullOrUndefined(positions)) {
      this.positions = [];
    } else {
      this.positions = positions.map((lp) => new EJSLTemplatePosition(lp, this));
    }

    const cssBlocks = xmi['cssblocks'] as any[];
    if (isNullOrUndefined(cssBlocks)) {
      this.cssBlocks = [];
    } else {
      this.cssBlocks = cssBlocks.map((lp) => new EJSLCssBlock(lp, this));
    }
  }
}
