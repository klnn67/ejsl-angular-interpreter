import {EJSLExtension} from './EJSLExtension';
import {EJSLPart} from '../EJSLPart';
import {EJSLPageRef} from '../EJSLPageRef';
import {EJSLModulePageRef} from '../EJSLModulePageRef';

export class EJSLModule extends EJSLExtension {
  pageRef: EJSLModulePageRef;

  constructor(xmi: any, part: EJSLPart) {
    super(xmi, part);
    this.pageRef = new EJSLModulePageRef(xmi.pageRef[0], this);
  }
}
