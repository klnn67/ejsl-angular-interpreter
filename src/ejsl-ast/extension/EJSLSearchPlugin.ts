import {EJSLPart} from '../EJSLPart';
import {EJSLLocalParameter} from '../EJSLLocalParameter';
import {EJSLPlugin} from './EJSLPlugin';
import {isNullOrUndefined} from 'util';
import {EJSLEntity} from '../EJSLEntity';

export class EJSLSearchPlugin extends EJSLPlugin {
  entitiyIndex: number;
  localParameters: EJSLLocalParameter[];

  constructor(xmi: any, part: EJSLPart) {
    super(xmi, part);

    this.entitiyIndex = parseInt((xmi.$.entities as string).split('.')[1], 10);

    const localParameters = xmi['localparameters'] as any[];
    if (isNullOrUndefined(localParameters)) {
      this.localParameters = [];
    } else {
      this.localParameters = localParameters.map((lp) => new EJSLLocalParameter(lp, this));
    }
  }

  getEntity(): EJSLEntity {
    return this.part.entities[this.entitiyIndex];
  }
}
