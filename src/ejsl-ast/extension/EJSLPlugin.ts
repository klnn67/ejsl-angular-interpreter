import {EJSLExtension} from './EJSLExtension';
import {EJSLPart} from '../EJSLPart';
import {EJSLPluginType} from '../types/EJSLPluginType';

export abstract class EJSLPlugin extends EJSLExtension {

  pluginType: EJSLPluginType;

  constructor(xmi: any, part: EJSLPart) {
    super(xmi, part);

    this.pluginType = EJSLPlugin.resolvePluginType(xmi);
  }

  static resolvePluginType(xmi: any): EJSLPluginType {
    const typeString = xmi.$['type'];
    switch (typeString) {
      case 'search':
        return EJSLPluginType.Search;
      default:
        throw new Error(`Unsupported PluginType: ${typeString}.`);
    }
  }
}
