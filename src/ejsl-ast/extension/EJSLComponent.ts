import {EJSLExtension} from './EJSLExtension';
import {EJSLSection} from '../EJSLSection';
import {EJSLPart} from '../EJSLPart';

export class EJSLComponent extends EJSLExtension {
  sections: EJSLSection[];

  constructor(xmi: any, part: EJSLPart) {
    super(xmi, part);

    const sections = xmi.sections as any[];
    if (sections) {
      this.sections = sections.map((s) => new EJSLSection(s, this));
    } else {
      this.sections = [];
    }
  }
}
