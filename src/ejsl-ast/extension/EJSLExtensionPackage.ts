import {EJSLExtension} from './EJSLExtension';
import {EJSLPart} from '../EJSLPart';
import {EJSLExtensionType} from '../types/EJSLExtensionType';
import {EJSLComponent} from './EJSLComponent';
import {EJSLModule} from './EJSLModule';

export class EJSLExtensionPackage extends EJSLExtension {
  extensions: EJSLExtension[];

  constructor(xmi: any, part: EJSLPart) {
    super(xmi, part);

    const extensions = xmi.extensions as any[];
    if (extensions) {
      this.extensions = extensions.map((e) => {
        switch (EJSLExtension.resolveType(e)) {
          case EJSLExtensionType.Component:
            return new EJSLComponent(e, part);
          case EJSLExtensionType.Module:
            return new EJSLModule(e, part);
          case EJSLExtensionType.ExtensionPackage:
            return new EJSLExtensionPackage(e, part);
        }
      });
    } else {
      this.extensions = [];
    }
  }
}
