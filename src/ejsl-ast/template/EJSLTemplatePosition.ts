import {EJSLTemplate} from '../extension/EJSLTemplate';
import {EJSLPositionType} from '../types/EJSLPositionType';
import {EJSLPositionParameter} from './EJSLPositionParameter';
import {isNullOrUndefined} from 'util';

export class EJSLTemplatePosition {
  template: EJSLTemplate;
  name: EJSLPositionType;
  parameters: EJSLPositionParameter[];

  constructor(xmi: any, template: EJSLTemplate) {
    this.template = template;
    this.name = EJSLTemplatePosition.resolvePositionType(xmi);

    const positionparameters = xmi['positionparameters'] as any[];
    if (isNullOrUndefined(positionparameters)) {
      this.parameters = [];
    } else {
      this.parameters = positionparameters.map((kv) => new EJSLPositionParameter(kv, this));
    }
  }

  static resolvePositionType(xmi: any): EJSLPositionType {
    const positionType = xmi.$['name'];
    switch (positionType) {
      case 'head':
        return EJSLPositionType.head;
      case 'contents':
        return EJSLPositionType.contents;
      case 'footer':
        return EJSLPositionType.footer;
      case 'left':
        return EJSLPositionType.left;
      case 'right':
        return EJSLPositionType.right;
      default:
        throw new Error(`Unsupported PositionType: ${positionType}.`);
    }
  }
}
