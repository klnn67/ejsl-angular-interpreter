import {EJSLTemplate} from '../extension/EJSLTemplate';
import {EJSLKeyValuePair} from '../EJSLKeyValuePair';
import {isNullOrUndefined} from 'util';

export class EJSLCssBlock {
  template: EJSLTemplate;
  selector: string;
  keyvaluepairs: EJSLKeyValuePair[];

  constructor(xmi: any, template: EJSLTemplate) {
    this.template = template;
    this.selector = xmi.$['selector'];

    const keyvaluepairs = xmi['keyvaluepairs'] as any[];
    if (isNullOrUndefined(keyvaluepairs)) {
      this.keyvaluepairs = [];
    } else {
      this.keyvaluepairs = keyvaluepairs.map((kv) => new EJSLKeyValuePair(kv));
    }
  }
}
