import {EJSLTemplatePosition} from './EJSLTemplatePosition';
import {EJSLPositionExtensionType} from '../types/EJSLPositionExtensionType';
import {EJSLKeyValuePair} from '../EJSLKeyValuePair';
import {isNullOrUndefined} from 'util';

export class EJSLPositionParameter {
  position: EJSLTemplatePosition;
  name: string;
  divid: string;
  type: EJSLPositionExtensionType;
  keyvaluepairs: EJSLKeyValuePair[];

  constructor(xmi: any, position: EJSLTemplatePosition) {
    this.position = position;
    this.name = xmi.$['name'];
    this.divid = xmi.$['divid'];
    this.type = EJSLPositionParameter.resolvePositionExtensionType(xmi);

    const keyvaluepairs = xmi['keyvaluepairs'] as any[];
    if (isNullOrUndefined(keyvaluepairs)) {
      this.keyvaluepairs = [];
    } else {
      this.keyvaluepairs = keyvaluepairs.map((kv) => new EJSLKeyValuePair(kv));
    }
  }

  static resolvePositionExtensionType(xmi: any): EJSLPositionExtensionType {
    const positionExtensionType = xmi.$['type'];
    switch (positionExtensionType) {
      case 'component':
        return EJSLPositionExtensionType.component;
      case 'modules':
        return EJSLPositionExtensionType.modules;
      default:
        throw new Error(`Unsupported PositionExtensionType: ${positionExtensionType}.`);
    }
  }
}
