import {EJSLSection} from './EJSLSection';
import {EJSLPage} from './page/EJSLPage';

export class EJSLPageRef {
  section: EJSLSection;
  pageIndex: number;

  constructor(xmi: any, section: EJSLSection) {
    this.section = section;

    this.pageIndex = parseInt((xmi.$.page as string).split('.')[1], 10);
  }

  getPage(): EJSLPage {
    return this.section.extension.part.pages[this.pageIndex];
  }
}
