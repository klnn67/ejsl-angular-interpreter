import {EJSLExtension} from './extension/EJSLExtension';
import {EJSLSectionType} from './types/EJSLSectionType';
import {EJSLPageRef} from './EJSLPageRef';

export class EJSLSection {
  extension: EJSLExtension;
  type: EJSLSectionType;
  pageRefs: EJSLPageRef[];

  constructor(xmi: any, extension: EJSLExtension) {
    this.extension = extension;
    this.type = EJSLSection.resolveType(xmi);

    const pageRefs = xmi.pageRef as any[];
    if (pageRefs) {
      this.pageRefs = pageRefs.map((p) => new EJSLPageRef(p, this));
    } else {
      this.pageRefs = [];
    }
  }

  static resolveType(xmi: any): EJSLSectionType {
    const typeString = xmi.$['xsi:type'];

    switch (typeString) {
      case 'eJSL:BackendSection':
        return EJSLSectionType.BackendSection;
      case 'eJSL:FrontendSection':
        return EJSLSectionType.FrontendSection;
      default:
        throw new Error(`Unsupported SectionType: ${typeString}.`);
    }
  }
}
