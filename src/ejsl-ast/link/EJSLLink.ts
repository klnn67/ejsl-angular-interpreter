import {EJSLPage} from '../page/EJSLPage';
import {EJSLLinkType} from '../page/EJSLLinkType';
import {EJSLAttribute} from '../EJSLAttribute';
import {isNullOrUndefined} from 'util';
import {EJSLPageAction} from '../EJSLPageAction';
import {EJSLPageType} from '../types/EJSLPageType';
import {EJSLDetailsPage} from '../page/EJSLDetailsPage';

export abstract class EJSLLink {
  page: EJSLPage;
  type: EJSLLinkType;

  linkedAttributeEntityIndex: number | null;
  linkedAttributeIndex: number | null;
  linkedActionIndex: number | null;
  name: string;
  targetPageIndex: number;

  constructor(xmi: any, page: EJSLPage) {
    this.page = page;
    this.type = EJSLLink.resolveLinkType(xmi);

    this.name = xmi.$.name;

    const linkedAttribute = xmi.$.linkedAttribute;
    if (linkedAttribute) {
      this.linkedAttributeEntityIndex = parseInt(linkedAttribute.split('/')[4].split('.')[1], 10);
      this.linkedAttributeIndex = parseInt(linkedAttribute.split('/')[5].split('.')[1], 10);
    }

    const linkedAction = xmi.$.linkedAction;
    if (linkedAction) {
      this.linkedActionIndex = parseInt(linkedAction.split('/')[5].split('.')[1], 10);
    }

    this.targetPageIndex = parseInt(xmi.$.target.split('/')[4].split('.')[1], 10);
  }

  static resolveLinkType(xmi: any): EJSLLinkType {
    const type = xmi.$['xsi:type'] as string;
    switch (type) {
      case 'eJSL:ContextLink':
        return EJSLLinkType.ContextLink;
      case 'eJSL:InternalLink':
        return EJSLLinkType.InternalLink;
      default:
        throw Error(`Unsupported LinkType: ${type}`);
    }
  }

  getLinkedAttribute(): EJSLAttribute | null {
    if (isNullOrUndefined(this.linkedAttributeEntityIndex)) {
      return null;
    } else {
      return this.page.part.entities[this.linkedAttributeEntityIndex].attributes[this.linkedAttributeIndex];
    }
  }

  getLinkedAction(): EJSLPageAction | null {
    if (isNullOrUndefined(this.linkedActionIndex)) {
      return null;
    } else {
      if (this.page.type === EJSLPageType.DetailsPage) {
        return (this.page as EJSLDetailsPage).pageActions[this.linkedActionIndex];
      }
    }
  }

  getTargetPage(): EJSLPage {
    return this.page.part.pages[this.targetPageIndex];
  }
}
