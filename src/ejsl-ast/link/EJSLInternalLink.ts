import {EJSLLink} from './EJSLLink';
import {EJSLPage} from '../page/EJSLPage';

export class EJSLInternalLink extends EJSLLink {
  constructor(xmi: any, page: EJSLPage) {
    super (xmi, page);
  }
}
