import {EJSLPage} from '../page/EJSLPage';
import {EJSLLink} from './EJSLLink';

export class EJSLContextLink extends EJSLLink {

  constructor(xmi: any, page: EJSLPage) {
    super(xmi, page);
  }
}
