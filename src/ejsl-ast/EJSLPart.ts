import {EJSLEntity} from './EJSLEntity';
import {EJSLModel} from './EJSLModel';
import {EJSLPage} from './page/EJSLPage';
import {isNullOrUndefined} from 'util';
import {EJSLPageType} from './types/EJSLPageType';
import {EJSLIndexPage} from './page/EJSLIndexPage';
import {EJSLDetailsPage} from './page/EJSLDetailsPage';
import {EJSLExtension} from './extension/EJSLExtension';
import {EJSLExtensionType} from './types/EJSLExtensionType';
import {EJSLComponent} from './extension/EJSLComponent';
import {EJSLExtensionPackage} from './extension/EJSLExtensionPackage';
import {EJSLModule} from './extension/EJSLModule';
import {EJSLPlugin} from './extension/EJSLPlugin';
import {EJSLPluginType} from './types/EJSLPluginType';
import {EJSLSearchPlugin} from './extension/EJSLSearchPlugin';
import {EJSLTemplate} from './extension/EJSLTemplate';

export class EJSLPart {
  model: EJSLModel;
  name: string;
  entities: EJSLEntity[];
  pages: EJSLPage[];
  extensions: EJSLExtension[];

  constructor(xmi: any, model: EJSLModel) {
    this.model = model;
    this.name = xmi.$['xsi:type'];

    const xmiEntities = xmi.feature['0'].entities as any[];
    if (!isNullOrUndefined(xmiEntities)) {
      this.entities = xmiEntities.map((e) => new EJSLEntity(e, this));
    }

    const xmiPages = xmi.feature['0'].pages as any[];
    if (!isNullOrUndefined(xmiPages)) {
      this.pages = (xmiPages as any[]).map((e) => {
        switch (EJSLPage.resolvePageType(e)) {
          case EJSLPageType.IndexPage:
            return new EJSLIndexPage(e, this);
          case EJSLPageType.DetailsPage:
            return new EJSLDetailsPage(e, this);
        }
      });
    } else {
      this.pages = [];
    }

    const extensions = xmi.extensions as any[];
    if (extensions) {
      this.extensions = extensions.map((e) => {
        const extensionType = EJSLExtension.resolveType(e);
        switch (EJSLExtension.resolveType(e)) {
          case EJSLExtensionType.Component:
            return new EJSLComponent(e, this);
          case EJSLExtensionType.Module:
            return new EJSLModule(e, this);
          case EJSLExtensionType.ExtensionPackage:
            return new EJSLExtensionPackage(e, this);
          case EJSLExtensionType.Template:
            return new EJSLTemplate(e, this);
          case EJSLExtensionType.Plugin:
            const pluginType = EJSLPlugin.resolvePluginType(e);
            switch (pluginType) {
              case EJSLPluginType.Search:
                return new EJSLSearchPlugin(e, this);
              default:
                throw new Error(`Unsupported PluginType: ${pluginType}`);
            }
          default:
            throw new Error(`Unsupported ExtensionType: ${extensionType}`);
        }
      });
    } else {
      this.extensions = [];
    }
  }
}
