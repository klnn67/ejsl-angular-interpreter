import {EJSLExtension} from './extension/EJSLExtension';
import {isNullOrUndefined} from 'util';
import {EJSLKeyValuePair} from './EJSLKeyValuePair';

export class EJSLLanguage {
  extension: EJSLExtension;
  name: string;
  keyvaluepairs: EJSLKeyValuePair[];

  constructor(xmi: any, extension: EJSLExtension) {
    this.extension = extension;
    this.name = xmi.$.name;

    const keyvaluepairs = xmi['keyvaluepairs'] as any[];
    if (isNullOrUndefined(keyvaluepairs)) {
      this.keyvaluepairs = [];
    } else {
      this.keyvaluepairs = keyvaluepairs.map((kv) => new EJSLKeyValuePair(kv));
    }
  }
}
