import {EJSLLanguage} from './EJSLLanguage';
import {EJSLExtension} from './extension/EJSLExtension';

export class EJSLKeyValuePair {
  name: string;
  value: string;

  constructor(xmi: any) {
    this.name = xmi.$['name'];
    this.value = xmi.$['value'];
  }
}
